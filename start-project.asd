;;;; start-project.asd

(asdf:defsystem #:start-project
  :serial t
  :description "Quickly start new Common Lisp projects."
  :author "Kyle Isom <coder@kyleisom.net>"
  :license "ISC licensed"
  :depends-on (#:quickproject
               #:getopt)
  :components ((:file "package")
               (:file "start-project")
               (:file "image")))
