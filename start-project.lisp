;;;; start-project.lisp

(in-package #:start-project)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; setup
(defvar *config-file* (merge-pathnames (user-homedir-pathname)
                                       #p ".start-project.lisp"))
(when (probe-file *config-file*)
  (load *config-file*))

;; do not override config file defaults
(defvar *default-author* "J. Random Lisper <nobody@localhost>")
(defvar *default-license* "ISC license")
(defvar *dependencies* (make-array 1 :fill-pointer 0))

(unless (equalp "SBCL" (lisp-implementation-type))
  (format t "~%~%[!] start-project currently only runs in sbcl.~%")
  (abort))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; internal functions

;; liberated from Peter Siebel's "Practical Common Lisp"
(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defmacro emptyp (a-string)
  `(string= ,a-string ""))

(defun validate-property (property default)
  (if (emptyp property)
      (if (emptyp default)
          nil
          default)
      property))

(defmacro get-property (property default)
  `(let* ((prompt (format nil "~A (~A)" ,property ,default))
           (answer (prompt-read prompt)))
      (validate-property answer ,default)))

(defun get-project-path ()
  (pathname (get-property "path to store project" "")))

(defun get-author ()
  (get-property "author" *default-author*))

(defun get-license ()
  (get-property "license" *default-license*))

(defun get-dependencies (dependency-list)
  (let ((dependency (get-property "package" "")))
    (unless (null dependency)
      (ql:quickload dependency)
      (vector-push-extend dependency dependency-list)
      (get-dependencies dependency-list))
    dependency-list))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; exported functions

(defun create-project (&optional (project ""))
  (let* ((project-path (if (emptyp project)
                           (get-project-path)
                           project))
         (author (get-author))
         (license (get-license))
         (dep-list (get-dependencies
                    (make-array 1
                                :fill-pointer 0
                                :adjustable t)))
         (depends `(,@(loop for package
                         across dep-list
                         collect (make-symbol (string-upcase package))))))
    (when (or (emptyp (namestring project-path))
              (emptyp author)
              (emptyp license))
      (format t "~%[!] invalid project metadata, aborting.~%")
      (return-from create-project))
    (let ((project-name
           (quickproject:make-project project-path
                                      :author author
                                      :license license
                                      :depends-on depends)))
      (if (emptyp project-name)
          (format t "[!] failed to create project!~~%")
          (progn
            (format t "~%~%[+] successfully created project ~A in ~A~%"
                    project-name project-path)
            (format t "    happy hacking!~%~%"))))))

(defun write-defaults ()
  (format t "This will completely overwrite the configuration file (~A)~%"
          *config-file*)
  (format t "Do you wish to continue? ")
  (unless (yes-or-no-p)
    (abort))
  (let ((author (get-author))
        (license (get-license)))
    (with-open-file (out *config-file*
                         :direction :output
                         :if-exists :supersede)
      (with-standard-io-syntax
        (print `(defparameter *default-author* ,author) out)
        (print `(defparameter *default-license* ,license) out)))))
