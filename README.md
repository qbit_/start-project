start-project
=============

Build an executable to quickly create new [Quicklisp](http://www.quicklisp.org)-based
projects.

START-PROJECT currently requires [SBCL](http://www.sbcl.org).

Examples
--------
To get started, you should have an environment set up similar to that detailed
in [Zach Beane's](http://www.xach.com) [intro to Quickproject](http://xach.livejournal.com/278047.html).

In summary, create the file `~/.config/common-lisp/source-registry.conf.d/projects.conf`
with the contents `(:tree (:home "code/lisp/"))` (Zach uses `src/lisp`, but my
lives in `~/code/`. Copy this project into that directory.

The next step is to load START-PROJECT from Quicklisp in SBCL:
    (ql:quickload :start-project)

Build the image with:

    (start-project:install-image "~/bin/start-project")

The resulting image comes with a help message:

    <ono-sendai: ~> $ start-project -h
    
    usage: start-project [-dh] [-i imagename]
    options:
        -d                set up defaults
        -h                print this help message
        -i image-name     create a new executable image at
                          image-name.
    
You can set the default name and license with the `-d` option. This will create
a configuation file in `~/.start-project.lisp`. 

A new image can be generated with start-project -i </path/to/image>. This is
generally not very useful, but is provided anyways.

New projects can be created by calling start-project without any options. For
example:

    <ono-sendai: ~> $ start-project
    path to store project (): ~/code/lisp/foo
    author (Kyle Isom <coder@kyleisom.net>): 
    license (ISC license): 
    package (): 
    WARNING: Coercing #P"~/code/lisp/foo" to directory
    
    
    [+] successfully created project foo in ~/code/lisp/foo
        happy hacking!

Alternatively, the paths to projects can be specified on the command line:

    <ono-sendai: ~> $ start-project ~/code/lisp/bar
    author (Kyle Isom <coder@kyleisom.net>): 
    license (ISC license): 
    package (): 
    WARNING: Coercing "/home/kyle/code/lisp/bar" to directory
    
    
    [+] successfully created project bar in /home/kyle/code/lisp/bar
        happy hacking!
    
Multiple projects can be specified as well:
    <ono-sendai: ~> $ start-project ~/code/lisp/baz ~/code/lisp/quux
    author (Kyle Isom <coder@kyleisom.net>): 
    license (ISC license): 
    package (): 
    WARNING: Coercing "/home/kyle/code/lisp/baz" to directory
    
    
    [+] successfully created project baz in /home/kyle/code/lisp/baz
        happy hacking!

    author (Kyle Isom <coder@kyleisom.net>): 
    license (ISC license): 
    package (): 
    WARNING: Coercing "/home/kyle/code/lisp/quux" to directory
    
    
    [+] successfully created project quux in /home/kyle/code/lisp/quux
        happy hacking!


Credits
-------
START-PROJECT is based on [Zach Beane's](http://www.xach.com/) 
[Quicklisp](http://www.quicklisp.org) and [Quickproject](http://www.xach.com/lisp/quickproject/)



License
-------
START-PROJECT is released under the ISC license:

    THE ISC LICENSE:
    --------------------------------------------------------------------------------
    
    the ISC license:
    Copyright (c) 2011, 2012 Kyle Isom <coder@kyleisom.net>
    
    Permission to use, copy, modify, and distribute this software for any
    purpose with or without fee is hereby granted, provided that the above 
    copyright notice and this permission notice appear in all copies.
    
    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 
    
    --------------------------------------------------------------------------------
    

Links
-----

* [Bitbucket page](https://bitbucket.org/kisom/start-project)
* [Tarball](http://lisp.kyleisom.net/dist/start-project/start-project.tgz)
* [Homepage](http://lisp.kyleisom.net/projects/start-project.html)
