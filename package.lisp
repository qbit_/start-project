;;;; package.lisp

(defpackage #:start-project
  (:use #:cl)
  (:export #:create-project
           #:write-default
           #:install-image
           #:cli-main))
