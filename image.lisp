;;;; function for building a saved image

(in-package :start-project)

(defparameter *getopt-args* '(("d" :none)
                              ("h" :none)
                              ("i" :required)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; internal functions
(defun main (argv)
  (let ((opts (nth-value 1 (getopt:getopt argv *getopt-args*)))
        (args (nth-value 0 (getopt:getopt argv *getopt-args*))))
    (if (null opts)
        (catch :END-OF-FILE
          (if (null args)
              (create-project)
              (dolist (project args)
                (format t "~%[+] creating ~A~%" project)
                (create-project project)))
          (sb-ext:quit))
        (dolist (opt opts)
          (case (car opt)
            ("d" (write-defaults))
            ("h" (help-message))
            ("i" (install-image (cdr opt)))
            (otherwise
             (format t "[!] invalid option!~%")
             (abort)))))))

(defun help-message ()
  (format t "~%usage: ~A [-dh] [-i imagename]~%" (car sb-ext:*posix-argv*))
  (format t "options:~%")
  (format t "    -d                set up defaults~%")
  (format t "    -h                print this help message~%")
  (format t "    -i image-name     create a new executable image at~%")
  (format t "                      image-name.~%~%")
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; exported functions
(defun install-image (image-path)
  (if (stringp image-path)
      (progn
        (format t "[+] writing to ~A~%" (pathname image-path))
        (sb-ext:save-lisp-and-die (pathname image-path)
                                  :executable t
                                  :toplevel #'cli-main))
      (progn
        (format t "~%~%[!] invalid image name!~%")
        (sb-ext:quit :unix-status 1))))

(defun cli-main ()
  (main (subseq sb-ext:*posix-argv* 1))
  (sb-ext:quit :unix-status 0))
